from django import forms
from .models import Usuario,Persona



class LoginForm(forms.Form):
    usuario = forms.CharField(required=True,widget=forms.EmailInput())
    clave = forms.CharField(required=True,widget=forms.PasswordInput())
    
    class Meta:
        model = Usuario